#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <numeric>
#include <algorithm>
#include <omp.h>

using namespace std;
int times = 0;
int ii = 0;
int GameOver = 1;
int bestActionMove;


int my_rank;   /* My process rank */



int listSuccessors(int house[], int successor[], bool isMaxPlayer)
{
	int i, start, end, count = 0;
	if (isMaxPlayer == 1)
	{
		start = 0; end = 5;
		for (i = start; i <= end; i++)
		{
			if (house[i] != 0)
			{
				successor[count] = i;
				count++;
			}
		}


		return count;
	}
	else //if (isMaxPlayer == 1)
	{
		start = 7; end = 12;
		for (i = start; i <= end; i++)
		{
			if (house[i] != 0)
			{
				successor[count] = i;
				count++;
			}
		}

		return count;
	}
}

int min(int a,int b){
	if(a<b){
		return a;
	}
	return b;
}
int max(int a,int b){
	if(a>b){
		return a;
	}
	return b;
}

const int houseLength = 14;

bool relocation(int nowHouse[], int pickedHouse, int nextHouse[]) {
	int houseIndex[] = { houseLength / 2 - 1, houseLength - 1 };
	int myHouse = pickedHouse / 7;
	int left = nowHouse[pickedHouse];
	int goHouse = pickedHouse + 1;

	for (int i = 0; i < houseLength; i++) {
		if (i == pickedHouse) {
			nextHouse[i] = 0;
			continue;
		}
		nextHouse[i] = nowHouse[i];
	}

	while (left) {
		goHouse = goHouse % houseLength;
		if (goHouse != houseIndex[!myHouse]) {
			nextHouse[goHouse]++;
			left--;
		}
		goHouse++;
	} goHouse--;

	int oppsite = houseLength - 2 - goHouse;
	if (oppsite != houseIndex[0] && oppsite != houseIndex[1])
		if (goHouse / 7 == myHouse)
			if (nextHouse[goHouse] == 1 && nextHouse[oppsite] >= 0) {
				nextHouse[houseIndex[myHouse]] += nextHouse[goHouse] + nextHouse[oppsite];
				nextHouse[goHouse] = nextHouse[oppsite] = 0;
			}

	if (goHouse == houseIndex[myHouse])
		return true;
	return false;
}


int evaluate(int house[])
{
	int value;
	value = house[13] - house[6];
	return value;
}


void finalScoring(int house[])
{
	int i, tmp;
	for (i = 0; i < 13; i++)
	{
		if (i == 6)
		{
			continue;
		}
		if (house[i] != 0 && i<6)
		{
			tmp = house[i];
			house[i] = 0;
			house[6] += tmp;
		}
		else if (house[i] != 0 && i > 6)
		{
			tmp = house[i];
			house[i] = 0;
			house[13] += tmp;
		}
	}


}


void minimax(int inputHouses[14], int depthMAX, int nowDepth, bool isMaxPlayer, bool canRelocation, int *mValue, int *p_alpha,int *p_beta, int PreviosPlayer)
{
	//printf("\n");
	
	int mValue_of_successor[6];
	for(int i=0;i<6;i++){
		if(isMaxPlayer){
			mValue_of_successor[i]=100;
		}
		else{
			mValue_of_successor[i]=-100;
		}
	}
	
	
	if (nowDepth >= depthMAX)
	{
		*mValue = evaluate(inputHouses);
		return;
	}


	int successor[14] = { 0 };
	
	int number_of_possible_actions = 0;
	
	number_of_possible_actions = listSuccessors(inputHouses, successor, isMaxPlayer); //listSuccessors
	
	if (number_of_possible_actions <= 0)											  //there is no successor
	{
		finalScoring(inputHouses);
		*mValue = evaluate(inputHouses);
		return;
	}


	int relocationHouses[6][14]={0};

	bool seed_fall_in_player_store[6] = { 0 };
	
	if (canRelocation)
	{	
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			seed_fall_in_player_store[i] = relocation(inputHouses, successor[i], relocationHouses[i]);
		}
	}
	else
	{
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			for (int j = 0; j <= 13; j++) {
				relocationHouses[i][j] = inputHouses[j];
			}
		}
	}
	
	int alpha = *p_alpha, beta = *p_beta;
	int *pp_alpha = &alpha, *pp_beta = &beta;
	int return_flag=0;
	
	
	
	if(nowDepth>0&&my_rank>0){
		#pragma omp parallel for if(nowDepth ==0) 	
		for (int i = 0; i < number_of_possible_actions; i++)
		{
			if(seed_fall_in_player_store[i]){
				minimax(relocationHouses[i], depthMAX, nowDepth + 1, isMaxPlayer, true, &mValue_of_successor[i] , pp_alpha , pp_beta , isMaxPlayer);
			}
			else{			
				minimax(relocationHouses[i], depthMAX, nowDepth + 1, !isMaxPlayer, true, &mValue_of_successor[i], pp_alpha , pp_beta , isMaxPlayer);		
			}	
			
			if(isMaxPlayer){
				*pp_beta=min(mValue_of_successor[i],*pp_beta);			
				if(*pp_beta <= *p_alpha){
					return_flag=1;
					i=6;
				}
			}
			else{
				*pp_alpha=max(mValue_of_successor[i],*pp_alpha);
				if(*p_beta <= *pp_alpha){
					return_flag=1;
					i=6;
				}
			}		
		}
		/*cout << endl;
		for(int k=0;k<6;k++){
			cout << " my_rank : " << my_rank << "nowDepth : " << nowDepth <<  "  :  " << mValue_of_successor[k] << endl;
		}
		cout << endl;*/
	}
	


	MPI_Status status;  	
    int left = 2 * (my_rank-1);
    int right = 2 * (my_rank-1) + 1;

	int i;
	
	if(nowDepth==0&&my_rank>0){
		//#pragma omp parallel for private(mValue_of_successor) num_threads(2)
		for (int i = left; i <= right; i++){
			if(seed_fall_in_player_store[i]){
				minimax(relocationHouses[i], depthMAX, nowDepth + 1, isMaxPlayer, true, &mValue_of_successor[i] , pp_alpha , pp_beta , isMaxPlayer);
			}
			else{			
				minimax(relocationHouses[i], depthMAX, nowDepth + 1, !isMaxPlayer, true, &mValue_of_successor[i], pp_alpha , pp_beta , isMaxPlayer);		
			}
			if(isMaxPlayer){
				*pp_beta=min(mValue_of_successor[i],*pp_beta);			
				if(*pp_beta <= *p_alpha){
					return_flag=1;
					i=6;
				}
			}
			else{
				*pp_alpha=max(mValue_of_successor[i],*pp_alpha);
				if(*p_beta <= *pp_alpha){
					return_flag=1;
					i=6;
				}
			}									
		}		
		MPI_Send(mValue_of_successor+left,1,MPI_INT,0,0,MPI_COMM_WORLD);
		MPI_Send(mValue_of_successor+left+1,1,MPI_INT,0,1,MPI_COMM_WORLD);
		//cout << "send_rank"<<my_rank<<" : "<< mValue_of_successor[left] << endl;
		//cout << "send_rank"<<my_rank<<" : "<< mValue_of_successor[left+1] << endl;
	}
	
	
	if(nowDepth==0&&my_rank==0){
		MPI_Recv(mValue_of_successor,1,MPI_INT,1,0,MPI_COMM_WORLD,&status);
		MPI_Recv(mValue_of_successor+1,1,MPI_INT,1,1,MPI_COMM_WORLD,&status);
		MPI_Recv(mValue_of_successor+2,1,MPI_INT,2,0,MPI_COMM_WORLD,&status);
		MPI_Recv(mValue_of_successor+3,1,MPI_INT,2,1,MPI_COMM_WORLD,&status);
		MPI_Recv(mValue_of_successor+4,1,MPI_INT,3,0,MPI_COMM_WORLD,&status);
		MPI_Recv(mValue_of_successor+5,1,MPI_INT,3,1,MPI_COMM_WORLD,&status);
		/*for(int i=0;i<6;i++){
			cout << "recv_rank : " <<mValue_of_successor[i] << " ";
		}
		cout << endl;*/
	}
	
	
	
	if(isMaxPlayer){
		*mValue = *pp_beta;
	}
	else{
		*mValue = *pp_alpha;
	}
	if(return_flag==1){
		return;
	}

	// 0 = max 1 = min	
	if (nowDepth == 0 && my_rank==0) {
		int bestActionIndex = 0;
		*mValue = mValue_of_successor[0];
		if (isMaxPlayer == 0)						//get mValue
		{
			for (int i = 0; i < number_of_possible_actions; i++)
			{
				
				if (mValue_of_successor[i] > *mValue)
				{
					*mValue = mValue_of_successor[i];
					bestActionIndex = i;
				}
			}
		}
		else
		{
			for (int i = 0; i < number_of_possible_actions; i++)
			{
				
				if (mValue_of_successor[i] < *mValue)
				{
					*mValue = mValue_of_successor[i];
					bestActionIndex = i;
				}
			}
		}
		
		cout << "best action:" << successor[bestActionIndex]<< endl;
		bestActionMove = successor[bestActionIndex];
	}
}

int main(int argc, char **argv)
{

	
    	
	int my_rank_;   /* My process rank */
	int z;         /* The number of processes*/ 	
		
    MPI_Init(&argc, &argv);
	MPI_Status status;  
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank_);
    MPI_Comm_size(MPI_COMM_WORLD, &z);
	
	my_rank= my_rank_;
	
	
	
	//---------------------
	int number_beans;
	int depthMAX, inputHouses[14]={0},turn;
	int GameOver = 1;
	//clock_t start, stop;
	//cout << "core number:"<<omp_get_num_procs << endl;
	if(my_rank==0)
	cout << "Number of beans : " ;
	//cin >> number_beans;	
	number_beans = atoi(argv[1]);
	for(int k=0;k<14;k++){
		if( k!=6 && k!=13){
			inputHouses[k] = number_beans;
		}
	}
	if(my_rank==0)
	cout << "me/opp:";
	//cin >> turn;
	turn = atoi(argv[2]);
	if(my_rank==0)
		cout << "Game Start:" << endl;
	//while(GameOver)
	//{
	int *p,opponent_choice,mValue = 0;
	p = &mValue;	
	if(my_rank==0){
		cout << "Opponent's Score:   " <<inputHouses[6]<< endl;
		for(int k=0;k<6;k++){
			cout << "   " << inputHouses[k];
		}
		cout << endl;
		cout << "My Score:           " <<inputHouses[13]<< endl;
		for(int k=7;k<13;k++){
			cout << "   " << inputHouses[k];
		}
		cout << endl;
	}

	if(turn){
		cout << "Opponent's turn" << endl;
		cout << "input opponent's choice:";
		cin >> opponent_choice;
		turn = relocation(inputHouses, opponent_choice, inputHouses);
	}
	else{
		int alpha = -100 , beta = 100;
		int *p_alpha = &alpha , *p_beta = &beta;
		if(my_rank==0)
			cout << "my turn" << endl;
		if(my_rank==0)
			cout << "input depth:";
		//cin >> depthMAX;
		depthMAX = atoi(argv[3]);			
		//start = clock();			
		minimax(inputHouses, depthMAX, 0, turn, true, p ,p_alpha,p_beta,turn);//minimax(int inputHouses[14], int depthMAX, int nowDepth, bool isMaxPlayer, bool canRelocation, int mValue)
		//stop = clock();
		//cout << "耗時秒數:"<<double(stop - start) / CLOCKS_PER_SEC <<endl;
		turn = relocation(inputHouses, bestActionMove, inputHouses);
		if(turn){
			turn=0;
		}else{
			turn=1;
		}
	}
	//}
	printf("\n");
	MPI_Finalize(); 
	return 0;
}
