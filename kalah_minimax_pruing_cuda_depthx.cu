#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <numeric>
#include <algorithm>
#include <math.h>
#include <string.h>
using namespace std;

int times = 0;
int ii = 0;
int GameOver = 1;
int bestActionMove;

int *array_cuda = NULL;

__host__ __device__ int listSuccessors(int house[], int successor[], bool isMaxPlayer)
{
	int i, start, end, count = 0;
	if (isMaxPlayer == 1)
	{
		start = 0; end = 5;
		for (i = start; i <= end; i++)
		{
			if (house[i] != 0)
			{
				successor[count] = i;
				count++;
			}
		}


		return count;
	}
	else //if (isMaxPlayer == 1)
	{
		start = 7; end = 12;
		for (i = start; i <= end; i++)
		{
			if (house[i] != 0)
			{
				successor[count] = i;
				count++;
			}
		}

		return count;
	}
}



const int houseLength = 14;

__host__ __device__ bool relocation(int nowHouse[], int pickedHouse, int nextHouse[]) {
	int houseIndex[] = { houseLength / 2 - 1, houseLength - 1 };
	int myHouse = pickedHouse / 7;
	int left = nowHouse[pickedHouse];
	int goHouse = pickedHouse + 1;

	for (int i = 0; i < houseLength; i++) {
		if (i == pickedHouse) {
			nextHouse[i] = 0;
			continue;
		}
		nextHouse[i] = nowHouse[i];
	}

	while (left) {
		goHouse = goHouse % houseLength;
		if (goHouse != houseIndex[!myHouse]) {
			nextHouse[goHouse]++;
			left--;
		}
		goHouse++;
	} goHouse--;

	int oppsite = houseLength - 2 - goHouse;
	if (oppsite != houseIndex[0] && oppsite != houseIndex[1])
		if (goHouse / 7 == myHouse)			// app >0   web >=0
			if (nextHouse[goHouse] == 1 && nextHouse[oppsite] > 0) {
				nextHouse[houseIndex[myHouse]] += nextHouse[goHouse] + nextHouse[oppsite];
				nextHouse[goHouse] = nextHouse[oppsite] = 0;
			}

	if (goHouse == houseIndex[myHouse])
		return true;
	return false;
}


__host__ __device__ int evaluate(int house[])
{
	int value;
	value = house[13] - house[6];
	return value;
}


__host__ __device__ void finalScoring(int house[])
{
	int i, tmp;
	for (i = 0; i < 13; i++)
	{
		if (i == 6)
		{
			continue;
		}
		if (house[i] != 0 && i<6)
		{
			tmp = house[i];
			house[i] = 0;
			house[6] += tmp;
		}
		else if (house[i] != 0 && i > 6)
		{
			tmp = house[i];
			house[i] = 0;
			house[13] += tmp;
		}
	}


}
__device__ void minimax(int inputHouses[14], int depthMAX, int nowDepth, bool isMaxPlayer, bool canRelocation, int *mValue, int *p_alpha,int *p_beta, int PreviosPlayer, int *k)
{
	//printf("\n");
	
	__shared__ int mValue_of_successor[6];
	for(int i=0;i<6;i++){
		if(isMaxPlayer){
			mValue_of_successor[i]=100;
		}
		else{
			mValue_of_successor[i]=-100;
		}
	}
	
	
	if (nowDepth >= depthMAX)
	{
		*mValue = evaluate(inputHouses);
		*k = *k+1;
		return;
	}


	int successor[14] = { 0 };
	int number_of_possible_actions = 0;
	number_of_possible_actions = listSuccessors(inputHouses, successor, isMaxPlayer); //listSuccessors
	if (number_of_possible_actions <= 0)											  //there is no successor
	{
		finalScoring(inputHouses);
		*mValue = evaluate(inputHouses);
		*k = *k+1;
		return;
	}


	int relocationHouses[6][14]={0};

	bool seed_fall_in_player_store[6] = { 0 };
	
	if (canRelocation)
	{	
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			seed_fall_in_player_store[i] = relocation(inputHouses, successor[i], relocationHouses[i]);
		}
	}
	else
	{
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			for (int j = 0; j <= 13; j++) {
				relocationHouses[i][j] = inputHouses[j];
			}
		}
	}
	
	int alpha = *p_alpha, beta = *p_beta;
	int *pp_alpha = &alpha, *pp_beta = &beta;
	int return_flag=0;
	
	for (int i = 0; i < number_of_possible_actions; i++)
	{
		if(seed_fall_in_player_store[i]){
			minimax(relocationHouses[i], depthMAX, nowDepth + 1, isMaxPlayer, true, &mValue_of_successor[i] , pp_alpha , pp_beta , isMaxPlayer,k);
		}
		else{			
			minimax(relocationHouses[i], depthMAX, nowDepth + 1, !isMaxPlayer, true, &mValue_of_successor[i], pp_alpha , pp_beta , isMaxPlayer,k);		
		}	
		
		if(isMaxPlayer){
			*pp_beta=min(mValue_of_successor[i],*pp_beta);			
			if(*pp_beta <= *p_alpha){
				return_flag=1;
				i=6;
			}
		}
		else{
			*pp_alpha=max(mValue_of_successor[i],*pp_alpha);
			if(*p_beta <= *pp_alpha){
				return_flag=1;
				i=6;
			}
		}		
	}

	// 0 = max 1 = min

	if(isMaxPlayer){
		*mValue = *pp_beta;
	}
	else{
		*mValue = *pp_alpha;
	}
	if(return_flag==1){
		return;
	}

	if (nowDepth == 0 ) {
		int bestActionIndex = 0;
		*mValue = mValue_of_successor[0];
		if (isMaxPlayer == 0)						//get mValue
		{
			for (int i = 0; i < number_of_possible_actions; i++)
			{
				
				if (mValue_of_successor[i] > *mValue)
				{
					*mValue = mValue_of_successor[i];
					bestActionIndex = i;
				}
			}
		}
		else
		{
			for (int i = 0; i < number_of_possible_actions; i++)
			{
				
				if (mValue_of_successor[i] < *mValue)
				{
					*mValue = mValue_of_successor[i];
					bestActionIndex = i;
				}
			}			
		}
		//*k = successor[bestActionIndex];
	}
}



__global__ void minimax_global(int depthMAX,int *size, int *d_result, int *d_array_cuda)
{
	int t =  blockIdx.x * blockDim.x + threadIdx.x ;
	if(t<*size){
		int a = -100 , b = 100 ,c =0 ,d =0;		
		int *d_mvalue = &c , *pp_alpha = &a,*pp_beta = &b , *z = &d;	
		int relocationHouses[14];
		int start = t*14;
		for(int i=0;i<14;i++){
				relocationHouses[i]=d_array_cuda[start+i];
		}
		minimax(relocationHouses, depthMAX, 1, 1, true, d_mvalue, pp_alpha , pp_beta , 1 , z);
		d_result[t]=*z;
	}
}


void minimax_serial(int inputHouses[14], int depthMAX, int nowDepth, bool isMaxPlayer, bool canRelocation, int *mValue , int pos ,int pos_i)
{
	//printf("\n");
	int current = pos * 6 + pos_i * 14;
	/*stringstream s;
	s << current;
	cout << s.str()<< endl;*/
	
	int mValue_of_successor[6] = { 0 };
	
	if (nowDepth >= depthMAX){
		for(int i=0;i<14;i++){
			array_cuda[current+i] = inputHouses[i];
		}
		return;
	}

	if (nowDepth >= depthMAX)
	{
		*mValue = evaluate(inputHouses);
		return;
	}


	int successor[14] = { 0 };
	int number_of_possible_actions = 0;
	number_of_possible_actions = listSuccessors(inputHouses, successor, isMaxPlayer); //listSuccessors
	if (number_of_possible_actions <= 0)											  //there is no successor
	{
		finalScoring(inputHouses);
		*mValue = evaluate(inputHouses);
		return;
	}


	int relocationHouses[6][14]={0};

	bool seed_fall_in_player_store[6] = { 0 };
	
	if (canRelocation)
	{	
	
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			seed_fall_in_player_store[i] = relocation(inputHouses, successor[i], relocationHouses[i]);
		}
	}
	else
	{
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			for (int j = 0; j <= 13; j++) {
				relocationHouses[i][j] = inputHouses[j];
			}
		}
	}
	
		
	for (int i = 0; i < number_of_possible_actions; i++)
	{
		
		if(seed_fall_in_player_store[i]){
			minimax_serial(relocationHouses[i], depthMAX, nowDepth + 2, isMaxPlayer, true, &mValue_of_successor[i] , current, i);
		}
		else{
			minimax_serial(relocationHouses[i], depthMAX, nowDepth + 1, !isMaxPlayer, true, &mValue_of_successor[i], current, i);
		}
	}
}



int main(int argc, char **argv)
{
	int number_beans;
	int depthMAX, inputHouses[14]={0},turn;
	//int GameOver = 1;
	//clock_t start, stop;
	//cout << "core number:"<<omp_get_num_procs << endl;
	cout << "Number of beans : " ;
	//cin >> number_beans;	
	number_beans = atoi(argv[1]);
	for(int k=0;k<14;k++){
		if( k!=6 && k!=13){
			inputHouses[k] = number_beans;
		}
	}
	cout << "me/opp:";
	//cin >> turn;
	turn = atoi(argv[2]);
	cout << "Game Start:" << endl;
	//while(GameOver)
	//{
		int *p = new int,opponent_choice,mValue = 0;
		p = &mValue;	
		
		cout << "Opponent's Score:   " <<inputHouses[6]<< endl;
		for(int k=0;k<6;k++){
			cout << "   " << inputHouses[k];
		}
		cout << endl;
		cout << "My Score:           " <<inputHouses[13]<< endl;
		for(int k=7;k<13;k++){
			cout << "   " << inputHouses[k];
		}
		cout << endl;

		if(turn){
			cout << "Opponent's turn" << endl;
			cout << "input opponent's choice:";
			cin >> opponent_choice;
			//turn = relocation(inputHouses, opponent_choice, inputHouses);
		}
		else{
			int k=0;
			int *count = &k;
			int depth_serial,depth_cuda,array_size;
			cout << "my turn" << endl;
			cout << "input depth:";
			//cin >> depthMAX;
			depthMAX = atoi(argv[3]);			
						
			depth_serial = 4;
			array_size = pow(6,depth_serial);
			cout << array_size << endl;
			depth_cuda = depthMAX - depth_serial;
			
			array_cuda = new int[array_size*14];
			
			
			minimax_serial(inputHouses, depth_serial, 0, turn, true, p ,0,0);
			
															
			int size = sizeof(int);	
			int *d_array_cuda = new int[array_size*14] , *d_result , result[array_size];
			
			//cudaDeviceSetLimit(cudaLimitMallocHeapSize,2000*sizeof(int));
			cudaDeviceSetLimit(cudaLimitStackSize,10000);	
			
			cudaError_t rc1 = cudaMalloc(&count, size);
			cudaError_t rc2 = cudaMalloc((void**)&d_result, size * array_size);
			cudaError_t rc3 = cudaMalloc((void**)&d_array_cuda, size * 14 * array_size);		
			
			cudaMemcpy(d_array_cuda, array_cuda, size * array_size * 14, cudaMemcpyHostToDevice);
			cudaMemcpy(count, &array_size, size, cudaMemcpyHostToDevice);
			
			
			
			if (rc1 != cudaSuccess || rc2 != cudaSuccess || rc3 != cudaSuccess)
				printf("Could not cudaMalloc memory: %s\n", cudaGetErrorString(rc3));	
			
			minimax_global<<<  46, 1024>>>(depth_cuda,count,d_result,d_array_cuda);	
		
			cudaError_t rc4 = cudaMemcpy(result, d_result, size*array_size, cudaMemcpyDeviceToHost);		
			if (rc4 != cudaSuccess)
				printf("Could not cudaMemcpy memory: %s\n", cudaGetErrorString(rc4));	
			

			int min = result[0], bestaction=0;
			for(int i=0;i<array_size;i++){
				if(result[i]<min){
					min = result[i];
					bestaction = i;
				}
				//cout << result[i] << endl;
			}
			//cout << "best action:"<<bestaction%6 + 7 << endl;
			for(int i=0;i<10;i++){
				cout << result[i] <<endl;
			}
						
			
			if(turn){
				turn=0;
			}else{
				turn=1;
			}
			
			delete [] array_cuda;
		}
	//}
	printf("\n");
	return 0;
}
