#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include <vector>
#include <numeric>
#include <algorithm>
using namespace std;

int times = 0;
int ii = 0;
int GameOver = 1;
int bestActionMove;

int *array_cuda = NULL;
struct para{
	int inputHouses[14];
	int depthMAX;
	int nowDepth;
	int isMaxPlayer;
};

__device__ int listSuccessors(int house[], int successor[], bool isMaxPlayer)
{
	int i, start, end, count = 0;
	if (isMaxPlayer == 1)
	{
		start = 0; end = 5;
		for (i = start; i <= end; i++)
		{
			if (house[i] != 0)
			{
				successor[count] = i;
				count++;
			}
		}


		return count;
	}
	else //if (isMaxPlayer == 1)
	{
		start = 7; end = 12;
		for (i = start; i <= end; i++)
		{
			if (house[i] != 0)
			{
				successor[count] = i;
				count++;
			}
		}

		return count;
	}
}



const int houseLength = 14;

__device__ bool relocation(int nowHouse[], int pickedHouse, int nextHouse[]) {
	int houseIndex[] = { houseLength / 2 - 1, houseLength - 1 };
	int myHouse = pickedHouse / 7;
	int left = nowHouse[pickedHouse];
	int goHouse = pickedHouse + 1;

	for (int i = 0; i < houseLength; i++) {
		if (i == pickedHouse) {
			nextHouse[i] = 0;
			continue;
		}
		nextHouse[i] = nowHouse[i];
	}

	while (left) {
		goHouse = goHouse % houseLength;
		if (goHouse != houseIndex[!myHouse]) {
			nextHouse[goHouse]++;
			left--;
		}
		goHouse++;
	} goHouse--;

	int oppsite = houseLength - 2 - goHouse;
	if (oppsite != houseIndex[0] && oppsite != houseIndex[1])
		if (goHouse / 7 == myHouse)			// app >0   web >=0
			if (nextHouse[goHouse] == 1 && nextHouse[oppsite] > 0) {
				nextHouse[houseIndex[myHouse]] += nextHouse[goHouse] + nextHouse[oppsite];
				nextHouse[goHouse] = nextHouse[oppsite] = 0;
			}

	if (goHouse == houseIndex[myHouse])
		return true;
	return false;
}


__device__ int evaluate(int house[])
{
	int value;
	value = house[13] - house[6];
	return value;
}


__device__  void finalScoring(int house[])
{
	int i, tmp;
	for (i = 0; i < 13; i++)
	{
		if (i == 6)
		{
			continue;
		}
		if (house[i] != 0 && i<6)
		{
			tmp = house[i];
			house[i] = 0;
			house[6] += tmp;
		}
		else if (house[i] != 0 && i > 6)
		{
			tmp = house[i];
			house[i] = 0;
			house[13] += tmp;
		}
	}


}
__device__ void minimax(int inputHouses[14], int depthMAX, int nowDepth, bool isMaxPlayer, bool canRelocation, int *mValue, int *p_alpha,int *p_beta, int PreviosPlayer, int *k)
{
	//printf("\n");
	
	int mValue_of_successor[6];
	for(int i=0;i<6;i++){
		if(isMaxPlayer){
			mValue_of_successor[i]=100;
		}
		else{
			mValue_of_successor[i]=-100;
		}
	}
	
	
	if (nowDepth >= depthMAX)
	{
		*mValue = evaluate(inputHouses);
		*k = *k+1;
		return;
	}


	int successor[14] = { 0 };
	int number_of_possible_actions = 0;
	number_of_possible_actions = listSuccessors(inputHouses, successor, isMaxPlayer); //listSuccessors
	if (number_of_possible_actions <= 0)											  //there is no successor
	{
		finalScoring(inputHouses);
		*mValue = evaluate(inputHouses);
		*k = *k+1;
		return;
	}


	int relocationHouses[6][14]={0};

	bool seed_fall_in_player_store[6] = { 0 };
	
	if (canRelocation)
	{	
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			seed_fall_in_player_store[i] = relocation(inputHouses, successor[i], relocationHouses[i]);
		}
	}
	else
	{
		for (int i = 0; i < number_of_possible_actions; i++) {                            //relocation
			for (int j = 0; j <= 13; j++) {
				relocationHouses[i][j] = inputHouses[j];
			}
		}
	}
	
	int alpha = *p_alpha, beta = *p_beta;
	int *pp_alpha = &alpha, *pp_beta = &beta;
	int return_flag=0;
	
	for (int i = 0; i < number_of_possible_actions; i++)
	{
		if(seed_fall_in_player_store[i]){
			minimax(relocationHouses[i], depthMAX, nowDepth + 1, isMaxPlayer, true, &mValue_of_successor[i] , pp_alpha , pp_beta , isMaxPlayer,k);
		}
		else{			
			minimax(relocationHouses[i], depthMAX, nowDepth + 1, !isMaxPlayer, true, &mValue_of_successor[i], pp_alpha , pp_beta , isMaxPlayer,k);		
		}	
		
		if(isMaxPlayer){
			*pp_beta=min(mValue_of_successor[i],*pp_beta);			
			if(*pp_beta <= *p_alpha){
				return_flag=1;
				i=6;
			}
		}
		else{
			*pp_alpha=max(mValue_of_successor[i],*pp_alpha);
			if(*p_beta <= *pp_alpha){
				return_flag=1;
				i=6;
			}
		}		
	}

	// 0 = max 1 = min

	if(isMaxPlayer){
		*mValue = *pp_beta;
	}
	else{
		*mValue = *pp_alpha;
	}
	if(return_flag==1){
		return;
	}

	if (nowDepth == 0 ) {
		int bestActionIndex = 0;
		*mValue = mValue_of_successor[0];
		if (isMaxPlayer == 0)						//get mValue
		{
			for (int i = 0; i < number_of_possible_actions; i++)
			{
				
				if (mValue_of_successor[i] > *mValue)
				{
					*mValue = mValue_of_successor[i];
					bestActionIndex = i;
				}
			}
		}
		else
		{
			for (int i = 0; i < number_of_possible_actions; i++)
			{
				
				if (mValue_of_successor[i] < *mValue)
				{
					*mValue = mValue_of_successor[i];
					bestActionIndex = i;
				}
			}			
		}
		*k = successor[bestActionIndex];
	}
}



__global__ void minimax_global(int depthMAX,int *mValue , int *p_alpha , int *p_beta,int *k)
{
	int relocationHouses[14];
	for(int i=0;i<14;i++){
		if(i==6||i==13){
			relocationHouses[i]=0;
		}else{
			relocationHouses[i]=7;
		}
	}
	*k = 0;
	minimax(relocationHouses, depthMAX, 0, 0, true, mValue, p_alpha , p_beta , 0 , k);
	*mValue = *k;
}





int main(int argc, char **argv)
{
	int number_beans;
	int depthMAX, inputHouses[14]={0},turn;
	//int GameOver = 1;
	//clock_t start, stop;
	//cout << "core number:"<<omp_get_num_procs << endl;
	//cout << "Number of beans : " ;
	//cin >> number_beans;	
	number_beans = atoi(argv[1]);
	for(int k=0;k<14;k++){
		if( k!=6 && k!=13){
			inputHouses[k] = number_beans;
		}
	}
	//cout << "me/opp:";
	//cin >> turn;
	turn = atoi(argv[2]);
	//cout << "Game Start:" << endl;
	//while(GameOver)
	//{
		int *p = new int,opponent_choice,mValue = 0;
		p = &mValue;	
		
		//cout << "Opponent's Score:   " <<inputHouses[6]<< endl;
		for(int k=0;k<6;k++){
			//cout << "   " << inputHouses[k];
		}
		//cout << endl;
		//cout << "My Score:           " <<inputHouses[13]<< endl;
		/*for(int k=7;k<13;k++){
			cout << "   " << inputHouses[k];
		}
		cout << endl;*/

		if(turn){
			//cout << "Opponent's turn" << endl;
			//cout << "input opponent's choice:";
			cin >> opponent_choice;
			//turn = relocation(inputHouses, opponent_choice, inputHouses);
		}
		else{
			int a = -100, b = 100, k=0;
			int *bestvalue = new int , *alpha = &a , *beta = &b ,*count = &k;
			int *p_alpha =  new int , *p_beta =  new int;
			//cout << "my turn" << endl;
			//cout << "input depth:";
			//cin >> depthMAX;	
			int size = sizeof(int);
			
			depthMAX = atoi(argv[3]);
			

			cudaDeviceSetLimit(cudaLimitMallocHeapSize,20000000*sizeof(double));
			cudaDeviceSetLimit(cudaLimitStackSize,129280);
			
			cudaError_t rc1 = cudaMalloc(&p, size);
			cudaError_t rc2 = cudaMalloc(&p_alpha, size);
			cudaError_t rc3 = cudaMalloc(&p_beta, size);
			cudaError_t rc5 = cudaMalloc(&count, size);

			cudaMemcpy(p_alpha, alpha, size, cudaMemcpyHostToDevice);
			cudaMemcpy(p_beta, beta, size, cudaMemcpyHostToDevice);
			
			if (rc1 != cudaSuccess || rc2 != cudaSuccess || rc3 != cudaSuccess)
				printf("Could not Memcpy memory: %s\n", cudaGetErrorString(rc1));
			
			
			minimax_global<<< 1 , 1 >>>(depthMAX,p,p_alpha,p_beta,count);
			//f<<<1, 1>>>(p, depthMAX);
			
			cudaError_t rc4 = cudaMemcpy(bestvalue, p, size, cudaMemcpyDeviceToHost);
			if (rc4 != cudaSuccess)
				printf("Could not Memcpy memory: %s\n", cudaGetErrorString(rc4));
			
			cout << "best_action:" << (*bestvalue)+7 << endl;
			
			if(turn){
				turn=0;
			}else{
				turn=1;
			}
		}
	//}
	printf("\n");
	return 0;
}
